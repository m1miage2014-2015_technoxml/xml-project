import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


public class ServletTest extends HttpServlet{

	  /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
    
    /** 
     * Simple transformation method. 
     * @param sourcePath - Absolute path to source xml file. 
     * @param xsltPath - Absolute path to xslt file. 
     * @param resultDir - Directory where you want to put resulting files. 
     */  
    public static void simpleTransform(String sourcePath, String xsltPath,  
                                       String resultDir) {  
        TransformerFactory tFactory = TransformerFactory.newInstance();  
        try {  
            Transformer transformer =  
                tFactory.newTransformer(new StreamSource(new File(xsltPath)));  
  
            transformer.transform(new StreamSource(new File(sourcePath)),  
                                  new StreamResult(new File(resultDir)));  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    }  
  
    public static void main(String[] args) {  
        //Set saxon as transformer.  
  
        simpleTransform("d:/project/hob/AppModule.xml",  
                        "d:/project/hob/create-fragment.xslt", "C:/");  
  
    } 
    
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.setProperty("javax.xml.transform.TransformerFactory",  
                           "net.sf.saxon.TransformerFactoryImpl");  
		
        response.setContentType("text/html"); 
		PrintWriter out = response.getWriter();
		
		DatabaseTest bdd = new DatabaseTest();
		String result = bdd.query("for $x in //ficheInscription where $x/lieuCodePostal=06200 return $x");
		
		File f = new File("tmp.xml");
		FileWriter fw = new FileWriter(f);
		fw.write(result);
		fw.close();
		
		
		out.println();
//		out.println("Nous sommes le " + (new java.util.Date()).toString()); 
		out.flush(); 
	}
}
