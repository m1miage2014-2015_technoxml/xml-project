
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseServer;

import java.io.File;

import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

import javax.xml.transform.Transformer;
import javax.xml.transform.Result;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;


public class ServletSearchingFormulaire extends HttpServlet  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The path to the stylesheet. */
	public final static String XSLT_PATH = "xsl/testSearch.xsl";


	private Transformer getTransformer(StreamSource streamSource) {
		// setup the xslt transformer
		net.sf.saxon.TransformerFactoryImpl impl = 
				new net.sf.saxon.TransformerFactoryImpl();

		try {
			return impl.newTransformer(streamSource);

		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		// si l'on demande de cr�er un PDF alors on le cr�e
		// sinon, c'est que l'on demande de lancer une nouvelle requete
		if(request.getParameter("createPDF") != null){


			// the XSL FO file
			File xsltfile = null;
			StreamSource source = null;
						
			if(request.getParameter("createPDF").equals("normal")){
				
				xsltfile = new File("src/main/webapp/xsl/ReponseSearchToPDF.xsl");
				source = new StreamSource(new File("src/main/webapp/xml/ReponseSearchXml.xml"));
				
			} else if(request.getParameter("createPDF").equals("detail")){

				xsltfile = new File("src/main/webapp/xsl/ReponseSearchToPDFDetaille.xsl");
				source = new StreamSource(new File("src/main/webapp/xml/ReponseSearchXmlDetaille.xml"));
				
			}
			
			// creation of transform source
			StreamSource transformSource = new StreamSource(xsltfile);
			// create an instance of fop factory
			FopFactory fopFactory = FopFactory.newInstance();
			// a user agent is needed for transformation
			FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
			// to store output
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();

			Transformer xslfoTransformer;
			try
			{
				xslfoTransformer = getTransformer(transformSource);

				// Construct fop with desired output format
				Fop fop;
				try {
					fop = fopFactory.newFop
							(MimeConstants.MIME_PDF, foUserAgent, outStream);
					// Resulting SAX events (the generated FO) 
					// must be piped through to FOP
					Result res = new SAXResult(fop.getDefaultHandler());

					// Start XSLT transformation and FOP processing
					try {
						// everything will happen here..
						xslfoTransformer.transform(source, res);

						// to write the content to out put stream
						byte[] pdfBytes = outStream.toByteArray();
						response.setContentLength(pdfBytes.length);
						response.setContentType("application/pdf");
						response.addHeader("Content-Disposition", "attachment;filename=pdffile.pdf");
						response.getOutputStream().write(pdfBytes);
						response.getOutputStream().flush();

					} catch (TransformerException e) {
						try {
							throw e;
						} catch (TransformerException e1) {
							e1.printStackTrace();
						}
					}
				} catch (FOPException e) {
					try {
						throw e;
					} catch (FOPException e1) {
						e1.printStackTrace();
					}
				}
			} catch (TransformerFactoryConfigurationError e) {
				throw e;
			}


			request.setAttribute("resultatRequete", "resultatTrouve");

			// Redirection vers la page search.jsp
			getServletContext().getRequestDispatcher("/search.jsp").forward(request, response);


			// on fait une recherche
		} else {

			// on veux plus d'infos sur un ev�nement en particulier
			// on le diff�rencie des autres gr�ce � son adresse
			if(request.getParameter("lieuAdresse")!=null && request.getParameter("lieuNom")!=null){

				String lieuAdresse = request.getParameter("lieuAdresse");
				String lieuNom = request.getParameter("lieuNom");
				
				String stringISOAdresse = null;
				String stringISONom = null;
		        try {
		        	stringISOAdresse = new String(lieuAdresse.getBytes("ISO-8859-1"), "UTF-8");
		        	stringISONom = new String(lieuNom.getBytes("ISO-8859-1"), "UTF-8");
		        } catch (java.io.UnsupportedEncodingException e) {

		        }
				
				
				String query="for $lieu in doc('src/main/webapp/xml/jep2014.xml')/programmeDataEdition/fichesInscription/ficheInscription "
						+ " where( "
						+ "$lieu/lieuAdresse"+ " =" + " " + '"' + stringISOAdresse +'"' + ""
						+ " and $lieu/lieuNom"+ " =" + " " + '"' + stringISONom +'"' + ""
						+ ") return $lieu";
				
				System.out.println("ServletSearchingFormulaire.doPost() " + query);
				
				// on envois la requete
//				String xml = new Database().executeQueryAndReturnXML(this, query);
				String xml = DatabaseServer.getInstance().executeQueryAndReturnXML(this, query);
				
				System.out.println("ServletSearchingFormulaire.doPost() " + xml);

				// on cr�� le fichier XML , pour pouvoir le tranmettre � la fonction simpleTransform(), avec son fichier XSLT
				try {
					OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream("src/main/webapp/xml/ReponseSearchXmlDetaille.xml"),"UTF-8");
					out.write("<?xml version='1.0' encoding='UTF-8'?>");
					out.write(xml);
					out.close();

				} catch (IOException e){
					e.printStackTrace();
				}

				
				request.setAttribute("resultatRequeteDetail", "resultatTrouve");
				request.setAttribute("requete", query);

				// Redirection vers la page search.jsp
				getServletContext().getRequestDispatcher("/search.jsp").forward(request, response);
				
				
			} else {


				// R�cup�rer les donn�es re�ues du formulaire
				String postalCode = (String) request.getParameter("lieuCodePostal/03");          

				String region = (String) request.getParameter("lieuRegion/06");          

				String accesTotalHandicape = (String) request.getParameter("lieuAccesTotalHandicapes/07");          

				String accesPartielHandicape = (String) request.getParameter("lieuAccesPartielHandicapes/08");          

				// Si aucun champ n'est remplis
				if ("".equals(postalCode) &&  "".equals(region) && 
						"".equals(accesTotalHandicape) && "".equals(accesPartielHandicape)) {

					request.setAttribute("erreur", "Vous devez remplir au moins l'un des champs recherch�.");
					// Redirection vers le formulaire form.jsp
					getServletContext().getRequestDispatcher("/search.jsp")
					.forward(request, response);
				}

				// Sinon
				else {

					// on regarde quels champs sont bien remplis
					Enumeration<String> listeNomParametrePasses = request.getParameterNames();

					ArrayList<String> listeBoutsRequete = new ArrayList<String>();

					while(listeNomParametrePasses.hasMoreElements()){
						String element = listeNomParametrePasses.nextElement();

						String[] contenuElement= element.split("/");

						if(request.getParameter(element)!=""){

							String valeurCherchee = contenuElement[0];
							String comparateur = "=";
							String valeurComparaison = request.getParameter(element);

							if(valeurComparaison.equals("Franche-Comté")){
								valeurComparaison = "Franche-Comt&#233;";
							} else if(valeurComparaison.equals("Midi-Pyrénées")){
								valeurComparaison = "Midi-Pyr&#233;n&#233;es";
							} else if(valeurComparaison.equals("Provence-Alpes-Côte d\'Azur")){
								valeurComparaison = "Provence-Alpes-C&#244;te d'azur";
							} else if(valeurComparaison.equals("Rhône-Alpes")){
								valeurComparaison = "Rh&#244;ne-Alpes";
							} else if(valeurComparaison.equals("La Réunion")){
								valeurComparaison = "La R&#233;union";
							}



							String boutRequete = "$lieu/"+valeurCherchee + " " + comparateur+" " + '"' + valeurComparaison +'"';

							listeBoutsRequete.add(boutRequete);
						}
					}


					// on �cris une requete par rapport aux champs donn�s et valeurs r�cup�r�es
					String query = "for $lieu in /programmeDataEdition/fichesInscription/ficheInscription"
							+ " where(";

					for(int i = 0; i < listeBoutsRequete.size(); i++){
						if(i == listeBoutsRequete.size()-1){
							query += listeBoutsRequete.get(i) + ") ";
						} else {
							query += listeBoutsRequete.get(i) + " and ";
						}
					}

					query +=" return "
							+ "<lieu> "
							+ "<nom> {data($lieu/lieuNom)}</nom>"
							+ "<address>{data($lieu/lieuAdresse)}</address>"
							+ "<codePostal>{data($lieu/lieuCodePostal)}</codePostal>"
							+ "<commune>{data($lieu/lieuCommune)}</commune>"
							+ "<numInsee>{data($lieu/lieuNumInsee)}</numInsee>"
							+ "<pays>{data($lieu/lieuPays)}</pays>"
							+ "<region>{data($lieu/lieuRegion)}</region>"
							+ "<accesTotalHandi>{data($lieu/lieuAccesTotalHandicapes)}</accesTotalHandi>"
							+ "<accesPartielHandi>{data($lieu/lieuAccesPartielHandicapes)}</accesPartielHandi>"
							+ "<lieuLatitude>{data($lieu/lieuLatitude)}</lieuLatitude>"
							+ "<lieuLongitude>{data($lieu/lieuLongitude)}</lieuLongitude>"
							+ " </lieu>";


					// on envois la requete
//					String xml = new Database().executeQueryAndReturnXML(this, query);
					String xml = DatabaseServer.getInstance().executeQueryAndReturnXML(this, query);

					String mauvaiseRequete = "for $lieu in /programmeDataEdition/fichesInscription/ficheInscription"
							+ " where($lieu/lieuCodePostal"+ " = " + '"' + "6546546" +'"'
							+ " ) return <lieu> </lieu>";
					String xmlVide = DatabaseServer.getInstance().executeQueryAndReturnXML(this, mauvaiseRequete);

					// on cr�� le fichier XML , pour pouvoir le tranmettre � la fonction simpleTransform(), avec son fichier XSLT
					try {
						OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream("src/main/webapp/xml/ReponseSearchXml.xml"),"UTF-8");
						out.write("<?xml version='1.0' encoding='UTF-8'?>");
						out.write(xml);
						out.close();

					} catch (IOException e){
						e.printStackTrace();
					}

					if(xml.equals(xmlVide)){
						request.setAttribute("erreur", "Aucun r�sultat trouv�.");
					} else {
						request.setAttribute("resultatRequete", "resultatTrouve");
					}
					

					// Redirection vers la page search.jsp
					getServletContext().getRequestDispatcher("/search.jsp").forward(request, response);

				}
			}
		}
	}
}
