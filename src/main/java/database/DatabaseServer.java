package database;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.basex.*;
import org.basex.core.BaseXException;
import org.basex.core.cmd.*;
import org.basex.query.QueryException;
import org.basex.query.QueryProcessor;
import org.basex.server.ClientQuery;
import org.basex.server.ClientSession;

public class DatabaseServer {
	
	private static DatabaseServer instance;
	private BaseXServer server;
	private ClientSession session;
	
	private DatabaseServer(){
	    try {
	    // Start server on default port 1984
			server = new BaseXServer();
		    // Create a client session with host name, port, user name and password
		    System.out.println("\n* Create a client session.");

		    this.session = new ClientSession("localhost", 1984, "admin", "admin");
		    
		      // Create a database
		      System.out.println("\n* Create a database.");

		      session.execute(new CreateDB("input", "src/main/webapp/xml/jep2014.xml"));
		      session.execute( new CreateIndex("fulltext"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized DatabaseServer getInstance() throws BaseXException{
		if(instance == null){
			instance = new DatabaseServer();
		}
		return instance;
	}
	
	public void executeQuery(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response, String query) throws IOException {

		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");

		StringBuilder result = new StringBuilder();

		result.append("<programmeDataEdition>\n");
		result.append("<fichesInscription>\n");

		
		System.out.println("\n* Run a query:");
	      
	    result.append(session.query(query).execute());
	     
		result.append("\n</fichesInscription>");
		result.append("\n</programmeDataEdition>");

		String resultXML = result.toString();

		response.getWriter().write(resultXML);
	}
	
	public synchronized String executeQueryAndReturnXML(HttpServlet servlet, String query) throws IOException{

		StringBuilder result = new StringBuilder();

		result.append("<programmeDataEdition>\n");
		result.append("<fichesInscription>\n");

		System.out.println("\n* Run a query:");
	      
	    result.append(session.query(query).execute());
	    
		result.append("\n</fichesInscription>");
		result.append("\n</programmeDataEdition>");

		String resultXML = result.toString();
		
		return resultXML;		
	}
	
	public void stopServer(){
		 try {
			server.stop();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
