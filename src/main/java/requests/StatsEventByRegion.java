package requests;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseServer;

/**
 * Servlet implementation class StatsAmenities
 */
@WebServlet("/StatsEventByRegion")
public class StatsEventByRegion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StatsEventByRegion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		String query = "for $lieu in /programmeDataEdition/fichesInscription/ficheInscription " +
				"let $region := $lieu/lieuRegion/text() " +
				"group by $region " +
				"let $count := count($lieu/lieuRegion/text()) " +
				"order by $count descending "+
				"return element { \"region\" } {attribute { \"count\" } { $count },$region}";
		
		DatabaseServer.getInstance().executeQuery(this, request, response, query);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

