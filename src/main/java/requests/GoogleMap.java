package requests;



import java.io.IOException;

import database.DatabaseServer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class GoogleMap
 */
@WebServlet("/GoogleMap")
public class GoogleMap extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GoogleMap() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String query = "for $lieu in /programmeDataEdition/fichesInscription/ficheInscription return "
				+ "<lieu>"
					+ "<latitude>{data($lieu/lieuLatitude)}</latitude>"
					+ "<longitude>{data($lieu/lieuLongitude)}</longitude>"
					+ "<nom>{data($lieu/lieuNom)}</nom>"
					+ "<website>{data($lieu/lieuSiteInternet)}</website>"
					+ "<address>{data($lieu/lieuAdresse)}</address>"
					+ "<insee>{data($lieu/lieuNumInsee)}</insee>"
				+ "</lieu>";
		DatabaseServer.getInstance().executeQuery(this, request, response, query);		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
