package requests;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseServer;

/**
 * Servlet implementation class StatsAmenities
 */
@WebServlet("/StatsClassifiedPlace")
public class StatsClassifiedPlace extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StatsClassifiedPlace() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
//		String query = "for $lieu in /programmeDataEdition/fichesInscription/ficheInscription "+
//				"let $b := $lieu/lieuClasseMH "+
//				"group by $b "+
//				"let $yes := count($lieu/lieuClasseMH/text()) where $lieu/lieuClasseMH/text()=\"1\" "+
//				"let $no := count(/programmeDataEdition/fichesInscription/ficheInscription)-$yes "+
//				"return <result><listed count=\"{$yes}\">Listed</listed><listed count=\"{$no}\">Not Listed</listed></result>";
		
		String query = "for $lieu in /programmeDataEdition/fichesInscription/ficheInscription "+
				"let $lieu1 := $lieu/lieuClasseMH "+
				"let $lieu2 := $lieu/lieuInscritMH "+
				"group by $lieu2, $lieu1 "+
				"let $c := count($lieu) where $lieu/lieuClasseMH/text()=\"1\" and $lieu/lieuInscritMH/text()=\"1\" "+
				"return "+
				"for $lieu in /programmeDataEdition/fichesInscription/ficheInscription "+
				"let $lieu1 := $lieu/lieuClasseMH "+
				"let $lieu2 := $lieu/lieuInscritMH "+
				"group by $lieu2, $lieu1 "+
				"let $c1 := count($lieu) where $lieu/lieuClasseMH/text()=\"1\" and $lieu/lieuInscritMH/text()=\"0\" "+
				"return "+
				"for $lieu in /programmeDataEdition/fichesInscription/ficheInscription "+
				"let $lieu1 := $lieu/lieuClasseMH "+
				"let $lieu2 := $lieu/lieuInscritMH "+
				"group by $lieu2, $lieu1 "+
				"let $c2 := count($lieu) where $lieu/lieuClasseMH/text()=\"0\" and $lieu/lieuInscritMH/text()=\"1\" "+
				"return "+
				"for $lieu in /programmeDataEdition/fichesInscription/ficheInscription "+
				"let $lieu1 := $lieu/lieuClasseMH "+
				"let $lieu2 := $lieu/lieuInscritMH "+
				"group by $lieu2, $lieu1 "+
				"let $c3 := count($lieu) where $lieu/lieuClasseMH/text()=\"0\" and $lieu/lieuInscritMH/text()=\"0\" "+
				"return <result>"+
				"<listed count=\"{$c}\">Listed and not registered</listed>"+
				"<listed count=\"{$c1}\">Listed and registered</listed>"+
				"<listed count=\"{$c2}\">Registered and not listed</listed>"+
				"<listed count=\"{$c3}\">Not listed and not registered</listed>"+
				"</result>";
		
		
		DatabaseServer.getInstance().executeQuery(this, request, response, query);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
