package requests;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import database.DatabaseServer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;

/**
 * Servlet implementation class Entry
 */
@WebServlet("/Entry")
public class Entry extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	public final static String
    /** The path to the stylesheet. */
    XSLT_PATH = "xsl/test.xsl";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Entry() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response, String nom) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String lieuAdresse = request.getParameter("lieuAdresse");
		String name = request.getParameter("nom");
		
		String stringISO = null;
		String stringISOName = null;
        try {
        	stringISO = new String ( lieuAdresse.getBytes(), "ISO-8859-1" );
        	stringISOName = new String ( name.getBytes(), "ISO-8859-1" );
        } catch (java.io.UnsupportedEncodingException e) {

        }
		
		
		String query="for $lieu in /programmeDataEdition/fichesInscription/ficheInscription"
				+ " where("
				+ "($lieu/lieuAdresse"+ " =" + '"' + stringISO +'"' +")"
				+ "and ($lieu/lieuNom"+ " =" + '"' + stringISOName +'"' +")"
				+ ") return $lieu";
		System.out.println(query);
		
		// on envois la requete
//		String xml = new Database().executeQueryAndReturnXML(this, query);
		String xml = DatabaseServer.getInstance().executeQueryAndReturnXML(this, query);
System.out.println(xml);
		// on cr�� le fichier XML , pour pouvoir le tranmettre � la fonction simpleTransform(), avec son fichier XSLT
		File f = new File("src/main/webapp/xml/detailsFromMap.xml");
		if(f.exists()){
			f.delete();
			f.createNewFile();
		}else{
			f.createNewFile();
		}
		try {
			OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(f),"UTF-8");
			out.write("<?xml version='1.0' encoding='UTF-8'?>");
			out.write(xml);
			out.close();

		} catch (IOException e){
			e.printStackTrace();
		}

		
//		request.setAttribute("resultatRequeteDetail", "resultatTrouve");
//		request.setAttribute("requete", query);

		// Redirection vers la page search.jsp
//		getServletContext().getRequestDispatcher("/mapData.jsp").forward(request, response);
		
		
		
//		String lieuNumInsee = request.getParameter("nom");
//		String query = "for $x in //ficheInscription where $x/lieuNumInsee=\""+ lieuNumInsee +"\" return $x";
//
//		String xml = new Database().executeQueryAndReturnXML(this, query);
//		
//    	System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
//    	
//    	processRequest(request, response, xml);
	}
	
	 /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, String xml)
    throws ServletException {
        ServletContext webApp = this.getServletContext();
        try {
        	 // Get concrete implementation
            TransformerFactory tFactory = TransformerFactory.newInstance();
            // Create a reusable templates for a particular stylesheet
            Templates templates = tFactory.newTemplates( new StreamSource( webApp.getRealPath( XSLT_PATH ) ) );
            // Create a transformer
            Transformer transformer = templates.newTransformer();

            // Get concrete implementation
            DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
            // Need a parser that support namespaces
            dFactory.setNamespaceAware( true );
            // Create the parser
            DocumentBuilder parser = dFactory.newDocumentBuilder();
            
            // Parse the XML document
    		String result = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n" + xml;
    		
    		File f = new File("tmp.xml");
    		FileWriter fw = new FileWriter(f);
    		fw.write(result);
    		fw.close();
    		
            Document doc = parser.parse( f );
            
            // Get the XML source
            Source xmlSource =  new DOMSource( doc );
            
            //Delete the tmp file
            f.delete();

            response.setContentType("text/html");
            
            // Transform input XML doc in HTML stream
            transformer.transform( xmlSource, new StreamResult( response.getWriter() ) );
            
            
        } catch (Exception ex) {
            throw new ServletException( ex );
        }
    }
}
