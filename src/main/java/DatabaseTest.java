import javax.management.Query;

import org.basex.core.*;
import org.basex.core.cmd.*;

public class DatabaseTest {
	
	private Context context;
	
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public DatabaseTest() throws BaseXException {
		// Database context.
	    Context context = new Context();

	    System.out.println("=== RunCommands ===");

	    // Create a database from a local or remote XML document or XML String
	    System.out.println("\n* Create a database.");

	    new CreateDB("DBProject", "src/main/resources/xml/jep2014.xml").execute(context);

	    this.context = context;
	    // Close and reopen the database
	    System.out.println("\n* Close and reopen database.");

	    new Close().execute(context);
	    new Open("DBProject").execute(context);

	    // Additionally create a full-text index
	    System.out.println("\n* Create a full-text index.");

	    new CreateIndex("fulltext").execute(context);

	    // Show information on the currently opened database
	    System.out.println("\n* Show database information:");

	    System.out.print(new InfoDB().execute(context));

	    System.out.println("-------  Query  --------");
	    
	    System.out.println(new XQuery("count(//ficheInscription)").execute(context));

	    System.out.println("-----------------");
	    // Drop indexes to save disk space
	    System.out.println("\n* Drop indexes.");

//	    new DropIndex("text").execute(context);
//	    new DropIndex("attribute").execute(context);
//	    new DropIndex("fulltext").execute(context);
//
//	    // Drop the database
//	    System.out.println("\n* Drop the database.");
//
//	    new DropDB("DBExample").execute(context);
//
//	    // Show all existing databases
//	    System.out.println("\n* Show existing databases:");
//
//	    System.out.print(new List().execute(context));
//
//	    // Close the database context
//	    context.close();
	}

	public String query(String XQuery) throws BaseXException{
		String result = new XQuery(XQuery).execute(this.context);
		return result;
	}
	
//	public static void main(final String[] args) throws BaseXException {
//		Database d = new Database();
//	}
}
