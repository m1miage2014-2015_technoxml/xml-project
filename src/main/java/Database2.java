

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.basex.BaseXClient;
import org.basex.core.cmd.CreateDB;
import org.basex.query.QueryException;
import org.basex.query.QueryProcessor;
import org.basex.core.BaseXException;
import org.basex.core.Context;

public class Database2 {

	private static Database2 instance;
	
	private BaseXClient session;
	
	public Context context;

	public Database2() throws BaseXException{
		this.context = new Context();
		new CreateDB("DBExample", "src/main/webapp/xml/entries_hotels.xml").execute(context);
		System.out.println("--------------------------------------------");
		System.out.println();
		System.out.println("CREATE BD");
		System.out.println();
		System.out.println("--------------------------------------------");
	}
	
	public static synchronized Database2 getInstance() throws BaseXException{
		if(instance == null){
			instance = new Database2();
		}
		return instance;
	}

	public synchronized void executeQuery(HttpServlet servlet, HttpServletRequest request,
			HttpServletResponse response, String query) throws IOException {

		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");

//		new CreateDB("DBExample", servlet.getServletContext().getRealPath(
//				"xml/jep2014.xml")).execute(context);
		
		QueryProcessor proc = new QueryProcessor(query, context);

		StringBuilder result = new StringBuilder();

		result.append("<programmeDataEdition>\n");
		result.append("<fichesInscription>\n");

		try {
			result.append(proc.execute());
		} catch (QueryException e) {
			e.printStackTrace();
		}
		result.append("\n</fichesInscription>");
		result.append("\n</programmeDataEdition>");

		String resultXML = result.toString();

		response.getWriter().write(resultXML);

		proc.close();

		context.close();
	}
	
	public synchronized String executeQueryAndReturnXML(HttpServlet servlet, String query) throws BaseXException{
		
		QueryProcessor proc = new QueryProcessor(query, context);

		StringBuilder result = new StringBuilder();

		result.append("<programmeDataEdition>\n");
		result.append("<fichesInscription>\n");

		try {
			result.append(proc.execute());
		} catch (QueryException e) {
			e.printStackTrace();
		}
		result.append("\n</fichesInscription>");
		result.append("\n</programmeDataEdition>");

		String resultXML = result.toString();

		proc.close();

		context.close();
		
		return resultXML;		
	}
}
