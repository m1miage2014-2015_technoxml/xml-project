<%@ page pageEncoding="UTF-8" %>
<%@ include file="header.html"%>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC6HzkG-VPkb5jQbxFkxUxZ3mAGZs5WOMA&sensor=false" type="text/javascript"></script>
<script src="js/markerclusterer_compiled.js"></script>
<script src="Saxonce/Saxonce.nocache.js"></script>

<link type="text/css" href="css/jquery-ui.css" rel="stylesheet" />
<script src="js/jquery-ui.js"></script>


<div class="container">
 	<div class="masthead">
		<%@ include file="searchingFormulaire.html"%>
 	</div>
</div>

</br>

<%
	String erreur = (String) request.getAttribute("erreur");
	String resultatRequete = (String) request.getAttribute("resultatRequete");
	String resultatRequeteDetail = (String) request.getAttribute("resultatRequeteDetail");
		   
    // Affichage du message s'il existe
    if (erreur != null || resultatRequete != null || resultatRequeteDetail != null) {
%>
		
				<%
				    // Affichage du message s'il existe
				    if (erreur != null) {
				%>
						<div class="container">
							<div class="masthead">
								<h3> Résultat </h3>
								</br>
						    	<strong>Erreur : <%= request.getAttribute("erreur")%> </strong>
					    	</div> 
				    	</div>
				<%
				    } else if (resultatRequeteDetail != null){
				%>    
						<div class="container">	
					    	<h3> Détail </h3>
					    	
					    	</br>
							<form method="post" action="servletSearching" class="form-horizontal" role="form">
								<input type="hidden" name="createPDF" value="detail">
								<button class='btn btn-primary btn-danger' type='submit'>PDF <span class='glyphicon glyphicon-print'></span></button>
							</form>
							
					    	<div id="result"></div>
							
							<!-- Script Saxon CE  -->
							<script type="application/xslt+xml" language="xslt2.0" src="./xsl/ReponseSearchXmlDetaille.xsl" data-source="./xml/ReponseSearchXmlDetaille.xml"></script>
						</div>
				<%
					} else if (resultatRequete != null) {
				%>
						<div class="container">
							<h3> Résultat </h3>
							</br>
							<form method="post" action="servletSearching" class="form-horizontal" role="form">
								<input type="hidden" name="createPDF" value="normal">
								<button class='btn btn-primary btn-danger' type='submit'>PDF <span class='glyphicon glyphicon-print'></span></button>
							</form>
							
							<div id="result"></div>
							
							<!-- Script Saxon CE  -->
							<script type="application/xslt+xml" language="xslt2.0" src="./xsl/ReponseSearchXml.xsl" data-source="./xml/ReponseSearchXml.xml"></script>
						
						</div>
				<%    	
				    }
				%>
<%    	
    }
%>

<%@ include file="footer.html"%>