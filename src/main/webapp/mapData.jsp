<%@ page pageEncoding="UTF-8" %>
<%@ include file="header.html"%>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC6HzkG-VPkb5jQbxFkxUxZ3mAGZs5WOMA&sensor=false" type="text/javascript"></script>
<script src="js/markerclusterer_compiled.js"></script>

<script src="Saxonce/Saxonce.nocache.js"></script>
<link href="css/map.css" rel="stylesheet">
<script src="js/jquery-1.11.1.js"></script>

<link type="text/css" href="css/jquery-ui.css" rel="stylesheet" />
<script src="js/jquery-ui.js"></script>


<script>
$(document).ready(
		function() {

		/* 	function getStars(standingsLevel) {
			var html="<div>",
			numberOfStars = standingsLevel.charAt(0);
			
			html+= '<p class="numberOfStars" style="display:none">'+numberOfStars+'</p>';
			for (var int = 0; int < numberOfStars; int++) {
				html += '<img src="images/star_icon.png" style="width:23px">';
			}
			html += '</div>';
			return html;
		} */

			var mapOptions = {
				center : new google.maps.LatLng(46.5, 2),				
				zoom : 6
			};
			map = new google.maps.Map(document.getElementById("googleMap"),
					mapOptions);

			$.get("GoogleMap", {}, function(data) {
				xmlDoc = $.parseXML(data);
				xmlData = $(xmlDoc);
				var markers = [];
				$(xmlData).find("lieu").each(
						function() {
							
							var data = $(this);
							lat = data.find("latitude").text();
							latitude = lat.replace(",", ".");
							lon = data.find("longitude").text();
							longitude = lon.replace(",", ".");
							var myLatlng = new google.maps.LatLng(
										parseFloat(latitude),
										parseFloat(longitude)
									);
							
							var contentString = '<div class="media"> <div class="media-body"> <h4 class="media-heading">'+data.find("nom").text()+'</h4> <ul class="list-group"><li class="list-group-item">Adresse : '+data.find("address").text()+'</li><li class="list-group-item">Site internet : <a href="'+data.find("website").text()+'">'+data.find("website").text()+'</a></li><li class="list-group-item"><button class="detail">Detail</button></li></ul> </div> </div>'
							var infowindow = new google.maps.InfoWindow({
							    content: contentString
							});
						 
							var marker = new google.maps.Marker({
								position : myLatlng,
								map : map,
								title : data.find("nom").text()
							});
							
							google.maps.event.addListener(marker, 'click', function() {
								    infowindow.open(map,marker);
								    $(".detail")
									.button()
									.on(
										"click",
										function(event){

									    	$("#result").contents().remove();
											$.ajax({
											    url: 'Entry',
											    type: 'POST',        
											    dataType: 'text',

											    data: {lieuAdresse: data.find("address").text(),
											    		nom: data.find("nom").text()},
											    success: function(data) {
													Saxon.run( { stylesheet: "./xsl/ReponseSearchXmlDetaille.xsl", source: "./xml/detailsFromMap.xml"});
											    														
													/* $("#result").append(

														    '<script type="application/xslt+xml" language="xslt2.0" src="./xsl/ReponseSearchXmlDetaille.xsl" data-source="./xml/detailsFromMap.xml">'+'</'+'script>'
													); */
													//equivalent using the procedural JavaScript API: 
													//var onSaxonLoad = function() { var xml = Saxon.requestXML(geo-files.xml); var xsl = Saxon.requestXML("display-geo.xsl"); var proc = new XSLT20Processor(xsl); proc.setParameter(null, "mass-kg", 225); proc.setParameter(null, "point", [128, 79]); proc.setParameter(null, "label", "Definitions"); proc.setErrorHandler(saxonHandler); proc.updateHTMLDocument(xml, null); } 
													
													
											    }
											});
											if($('#result').length != 0){
												$('#result').dialog({
													 autoOpen: true,
												     title: "Details",
													 height: "850",
													 width: "700",
													 modal: true,});
												$('html,body').animate(
					                	            	{
					        	                    	 	scrollTop: $('#result').offset().top
					        	                     	},
					        	                     	100
					        	                    );
											}
										}
									);
								  });
							 
							markers.push(marker);
						});
				var mc = new MarkerClusterer(map,markers);
			});
			
		});
		

</script>

<!-- <script>
	$.get("GoogleMap", {"Musée National du Sport"}, function(data) {
		$(".entry").append(data);
	});
</script> -->



      <div id="googleMap"></div>
    <br/>
    <br/>
    <br/>
	<div id="result"></div>

<%@ include file="footer.html"%>