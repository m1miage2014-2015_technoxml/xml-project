<?xml version="1.0" encoding="ISO-8859-1" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output method="html"/>
	
	<xsl:template match="/">
        <html>
            <head>
                <title>Test Appli</title>
            </head>
            <body>
                <h1><b><xsl:value-of select="//ficheInscription/lieuNom"></xsl:value-of></b></h1>
                <i><xsl:value-of select="//ficheInscription/lieuAdresse"></xsl:value-of></i><br/>
                <i><xsl:value-of select="//ficheInscription/lieuCodePostal"></xsl:value-of></i><br/>
                <i><xsl:value-of select="//ficheInscription/lieuCommune"></xsl:value-of></i><br/>
            </body>
        </html>
    </xsl:template>
  </xsl:stylesheet>
    