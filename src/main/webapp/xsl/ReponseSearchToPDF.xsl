<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns:fo="http://www.w3.org/1999/XSL/Format"
	exclude-result-prefixes="fo">
<xsl:template match="programmeDataEdition">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <fo:layout-master-set>
    <fo:simple-page-master master-name="my-page">
      <fo:region-body margin="1in"/>
    </fo:simple-page-master>
  </fo:layout-master-set>
  
	  <fo:page-sequence master-reference="my-page">
	    <fo:flow flow-name="xsl-region-body">
	      <fo:block>
	       <xsl:for-each select="./fichesInscription/lieu">
	      	<fo:table space-after="10mm">
	      		 <fo:table-body>
	      		 	<fo:table-row>
	      		 		<fo:table-cell>
	      		 			<fo:block>
	      		 				<xsl:value-of select="nom"/>
	      		 			</fo:block>
	      		 		</fo:table-cell>
	      		 	</fo:table-row>
	      		 	<fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Adresse
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="address" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Code Postal
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="codePostal" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Commune
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="commune" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Numéro Insee
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="numInsee" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Pays
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="pays" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Région
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="region" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Nombre accès total Handicapés
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="accesTotalHandi" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Nombre accès partiel Handicapés
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="accesPartielHandi" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	      		 </fo:table-body>
	      	</fo:table>	
	      	</xsl:for-each>    
	      </fo:block>
	    </fo:flow>
	  </fo:page-sequence>
</fo:root>
</xsl:template>
</xsl:stylesheet>