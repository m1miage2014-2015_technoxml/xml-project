<?xml version="1.0" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<xsl:result-document href="#result">
			<xsl:for-each
				select="programmeDataEdition/fichesInscription/ficheInscription">
				<table class='table table-striped table-bordered'>
					<thead>
						<tr>
							<th COLSPAN="2">
								<xsl:value-of select="lieuNom" />
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="40%">Adress</td>
							<td width="60%">
								<xsl:value-of select="lieuAdresse" />
							</td>
						</tr>
						<tr>
							<td width="40%">Area Code</td>
							<td width="60%">
								<xsl:value-of select="lieuCodePostal" />
							</td>
						</tr>
						<tr>
							<td width="40%">Town</td>
							<td width="60%">
								<xsl:value-of select="lieuCommune" />
							</td>
						</tr>
						<tr>
							<td width="40%">Insee Number</td>
							<td width="60%">
								<xsl:value-of select="lieuNumInsee" />
							</td>
						</tr>
						<tr>
							<td width="40%">Country</td>
							<td width="60%">
								<xsl:value-of select="lieuPays" />
							</td>
						</tr>
						<tr>
							<td width="40%">Region</td>
							<td width="60%">
								<xsl:value-of select="lieuRegion" />
							</td>
						</tr>
						<xsl:for-each select="sitesInternet/lieuSiteInternet">
							<tr>
								<td width="40%">Website</td>
								<td width="60%">
									<xsl:element name="a">
									  <xsl:attribute name="href"><xsl:apply-templates/></xsl:attribute>
										<xsl:apply-templates/>
									</xsl:element>
									
								</td>
							</tr>
						</xsl:for-each>
						<tr>
							<td width="40%">Number of full access for disabled</td>
							<td width="60%">
								<xsl:value-of select="lieuAccesTotalHandicapes" />
							</td>
						</tr>
						<tr>
							<td width="40%">Number of partial access for disabled</td>
							<td width="60%">
								<xsl:value-of select="lieuAccesPartielHandicapes" />
							</td>
						</tr>
						<tr>
							<td width="40%">Historic monuments listed site</td>
							<td width="60%">
								<xsl:value-of select="lieuClasseMH" />
							</td>
						</tr>
						<tr>
							<td width="40%">Historic monuments registered site</td>
							<td width="60%">
								<xsl:value-of select="lieuInscritMH" />
							</td>
						</tr>
						<tr COLSPAN="2">
							Featured Offers :
						</tr>
						<xsl:for-each select="offres/offre">
							<tr>
								<td width="40%">Offer </td>
								<td width="60%">
									<xsl:value-of select="./titre" />
								</td>
							</tr>
							<tr>
								<td width="40%">Type of guided tour.</td>
								<td width="60%">
									<xsl:value-of select="./typeVisiteCommentee" />
								</td>
							</tr>
							<tr>
								<td width="40%">Start date</td>
								<td width="60%">
									<xsl:value-of select="./dateDebut" />
								</td>
							</tr>
							<tr>
								<td width="40%">End date</td>
								<td width="60%">
									<xsl:value-of select="./dateFin" />
								</td>
							</tr>
							<tr>
								<td width="40%">Schedule</td>
								<td width="60%">
									<xsl:value-of select="./horaires" />
								</td>
							</tr>
							<tr>
								<td width="40%">Free</td>
								<td width="60%">
									<xsl:value-of select="./gratuit" />
								</td>
							</tr>
							<tr>
								<td width="40%">Registration required</td>
								<td width="60%">
									<xsl:value-of select="./inscriptionNecessaire" />
								</td>
							</tr>
							<tr>
								<td width="40%">Invitational</td>
								<td width="60%">
									<xsl:value-of select="./surInvitation" />
								</td>
							</tr>
							<xsl:for-each select="./themes/theme">
								<tr>
									<td width="40%">Theme </td>
									<td width="60%">
										<xsl:value-of select="." />
									</td>
								</tr>
							</xsl:for-each>
						</xsl:for-each>
					</tbody>
				</table>
			</xsl:for-each>
		</xsl:result-document>
	</xsl:template>
</xsl:stylesheet>
