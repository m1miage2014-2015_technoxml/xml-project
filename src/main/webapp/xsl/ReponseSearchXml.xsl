<?xml version="1.0" ?>
<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
  
	<xsl:template match="/">
	<xsl:result-document href="#result">
		<xsl:for-each select="programmeDataEdition/fichesInscription/lieu">
			<table class='table table-striped table-bordered'>
				<thead>
					<tr>
						<th COLSPAN="2">
							<form method="post" action="servletSearching" class="form-horizontal" role="form">
								<input type="hidden" name="lieuAdresse" value="{address}" />
								<input type="hidden" name="lieuNom" value="{nom}" />
								<button class='btn btn-block btn-primary btn-info' type='submit'>
									<xsl:value-of select="nom" />
								</button>
								
							</form>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td width="40%">Adress</td>
						<td width="60%">
							<xsl:value-of select="address" />
						</td>
					</tr>
					<tr>
						<td width="40%">Area Code</td>
						<td width="60%">
							<xsl:value-of select="codePostal" />
						</td>
					</tr>
					<tr>
						<td width="40%">Town</td>
						<td width="60%">
							<xsl:value-of select="commune" />
						</td>
					</tr>
					<tr>
						<td width="40%">Insee Number</td>
						<td width="60%">
							<xsl:value-of select="numInsee" />
						</td>
					</tr>
					<tr>
						<td width="40%">Country</td>
						<td width="60%">
							<xsl:value-of select="pays" />
						</td>
					</tr>
					<tr>
						<td width="40%">Region</td>
						<td width="60%">
							<xsl:value-of select="region" />
						</td>
					</tr>
					<tr>
						<td width="40%">Number of full access for disabled</td>
						<td width="60%">
							<xsl:value-of select="accesTotalHandi" />
						</td>
					</tr>
					<tr>
						<td width="40%">Number of partial access for disabled</td>
						<td width="60%">
							<xsl:value-of select="accesPartielHandi" />
						</td>
					</tr>
				</tbody>
			</table>
		</xsl:for-each>
		</xsl:result-document>
	</xsl:template>

</xsl:stylesheet>
