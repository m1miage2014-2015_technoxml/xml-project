<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
		xmlns:fo="http://www.w3.org/1999/XSL/Format"
	exclude-result-prefixes="fo">
<xsl:template match="programmeDataEdition">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <fo:layout-master-set>
    <fo:simple-page-master master-name="my-page">
      <fo:region-body margin="1in"/>
    </fo:simple-page-master>
  </fo:layout-master-set>
  
	  <fo:page-sequence master-reference="my-page">
	    <fo:flow flow-name="xsl-region-body">
	      <fo:block>
	       <xsl:for-each select="./fichesInscription/ficheInscription">
	      	<fo:table space-after="10mm">
	      		 <fo:table-body>
	      		 	<fo:table-row>
	      		 		<fo:table-cell>
	      		 			<fo:block>
	      		 				<xsl:value-of select="lieuNom"/>
	      		 			</fo:block>
	      		 		</fo:table-cell>
	      		 	</fo:table-row>
	      		 	<fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Adresse
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="lieuAdresse" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Code Postal
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="lieuCodePostal" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Commune
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="lieuCommune" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Numéro Insee
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="lieuNumInsee" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Pays
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="lieuPays" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Région
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="lieuRegion" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <xsl:for-each select="sitesInternet/lieuSiteInternet">
		                <fo:table-row>
		                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
		                		<fo:block>
		                    		Site Internet
		                  		</fo:block>
		                	</fo:table-cell>
		                	<fo:table-cell border="solid 1px black" text-align="center" >
		                		<fo:block>
		                    		<xsl:apply-templates/>
		                  		</fo:block>
		                	</fo:table-cell>
		                </fo:table-row>
	                </xsl:for-each>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Nombre accès total Handicapés
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="lieuAccesTotalHandicapes" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Lieu classé Monument Historique
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="lieuClasseMH" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                <fo:table-row>
	                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
	                		<fo:block>
	                    		Lieu inscrit Monument Historique
	                  		</fo:block>
	                	</fo:table-cell>
	                	<fo:table-cell border="solid 1px black" text-align="center" >
	                		<fo:block>
	                    		<xsl:value-of select="lieuInscritMH" />
	                  		</fo:block>
	                	</fo:table-cell>
	                </fo:table-row>
	                
	                <fo:table-row>
	      		 		<fo:table-cell>
	      		 			<fo:block>
	      		 				Offres Proposées : 
	      		 			</fo:block>
	      		 		</fo:table-cell>
	      		 	</fo:table-row>
	                <xsl:for-each select="offres/offre">
		                <fo:table-row>
		                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
		                		<fo:block>
		                    		Offre
		                  		</fo:block>
		                	</fo:table-cell>
		                	<fo:table-cell border="solid 1px black" text-align="center" >
		                		<fo:block>
		                    		<xsl:value-of select="./titre" />
		                  		</fo:block>
		                	</fo:table-cell>
		                </fo:table-row>
		                <fo:table-row>
		                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
		                		<fo:block>
		                    		Type de visite Commentée
		                  		</fo:block>
		                	</fo:table-cell>
		                	<fo:table-cell border="solid 1px black" text-align="center" >
		                		<fo:block>
		                    		<xsl:value-of select="./typeVisiteCommentee" />
		                  		</fo:block>
		                	</fo:table-cell>
		                </fo:table-row>
		                <fo:table-row>
		                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
		                		<fo:block>
		                    		Date début
		                  		</fo:block>
		                	</fo:table-cell>
		                	<fo:table-cell border="solid 1px black" text-align="center" >
		                		<fo:block>
		                    		<xsl:value-of select="./dateDebut" />
		                  		</fo:block>
		                	</fo:table-cell>
		                </fo:table-row>
		                <fo:table-row>
		                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
		                		<fo:block>
		                    		Date fin
		                  		</fo:block>
		                	</fo:table-cell>
		                	<fo:table-cell border="solid 1px black" text-align="center" >
		                		<fo:block>
		                    		<xsl:value-of select="./dateFin" />
		                  		</fo:block>
		                	</fo:table-cell>
		                </fo:table-row>
		                <fo:table-row>
		                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
		                		<fo:block>
		                    		Horaires
		                  		</fo:block>
		                	</fo:table-cell>
		                	<fo:table-cell border="solid 1px black" text-align="center" >
		                		<fo:block>
		                    		<xsl:value-of select="./horaires" />
		                  		</fo:block>
		                	</fo:table-cell>
		                </fo:table-row>
		                <fo:table-row>
		                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
		                		<fo:block>
		                    		Gratuit
		                  		</fo:block>
		                	</fo:table-cell>
		                	<fo:table-cell border="solid 1px black" text-align="center" >
		                		<fo:block>
		                    		<xsl:value-of select="./gratuit" />
		                  		</fo:block>
		                	</fo:table-cell>
		                </fo:table-row>
		                <fo:table-row>
		                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
		                		<fo:block>
		                    		Inscription nécessaire 
		                  		</fo:block>
		                	</fo:table-cell>
		                	<fo:table-cell border="solid 1px black" text-align="center" >
		                		<fo:block>
		                    		<xsl:value-of select="./inscriptionNecessaire" />
		                  		</fo:block>
		                	</fo:table-cell>
		                </fo:table-row>
		                <fo:table-row>
		                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
		                		<fo:block>
		                    		Sur invitation
		                  		</fo:block>
		                	</fo:table-cell>
		                	<fo:table-cell border="solid 1px black" text-align="center" >
		                		<fo:block>
		                    		<xsl:value-of select="./surInvitation" />
		                  		</fo:block>
		                	</fo:table-cell>
		                </fo:table-row>
		                <xsl:for-each select="./themes/theme">
			                <fo:table-row>
			                	<fo:table-cell border="solid 1px black" text-align="center" font-weight="bold">
			                		<fo:block>
			                    		Thème
			                  		</fo:block>
			                	</fo:table-cell>
			                	<fo:table-cell border="solid 1px black" text-align="center" >
			                		<fo:block>
			                    		<xsl:value-of select="." />
			                  		</fo:block>
			                	</fo:table-cell>
			                </fo:table-row>
		                </xsl:for-each>
	                </xsl:for-each>
	      		 </fo:table-body>
	      	</fo:table>	
	      	</xsl:for-each>    
	      </fo:block>
	    </fo:flow>
	  </fo:page-sequence>
</fo:root>
</xsl:template>
</xsl:stylesheet>