<%@ include file="header.html"%>

<script src="js/highcharts.js"></script>
<script src="js/highcharts-more.js"></script>
<script src="js/exporting.js"></script>
<script>
$(document).ready(function() {
	var options = {
			chart: {
				renderTo: 'pie',
				defaultSeriesType: 'pie',
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false
			},
			credits: {
				enabled: false
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			title: {
				text: 'Historic monuments listed and registered site'
			},
			xAxis: {
				title: {
					text: 'Historic monuments listed and registered site'
				},
				categories: []
			},
			yAxis: {
				title: {
					text: ''
				}
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: true
					},
					showInLegend: true
				}
			},
			series: []
	};
	
	
	
	// Callback for stats language
	var classifiedPlace = function(xml) {

		var $xml = $(xml);
		var seriesOptions = {
				type: 'pie',
				name: "Listed places",
				data: []
		};

		// push categories
		$xml.find('listed').each(function(i, category) {
			seriesOptions.data.push([$(category).text(),parseInt($(category).attr('count'))]);

		});

		options.series.push(seriesOptions);
		new Highcharts.Chart(options);
	};

	
	
	// CHART BAR
	var optionsBar = {
			chart: {
				renderTo: 'bar',
				type: 'bar'
			},
			credits: {
				enabled: false
			},
			title: {
				text: 'Number of event by region'
			},
			xAxis: {
				title: {
					text: 'Region'
				},
				categories: []
			},
			yAxis: {
				title: {
					text: 'Number'
				}
			},
			plotOptions: {
				bar: {
					dataLabels: {
						enabled: true
					}
				}
			},
			series: []
	};
	
	
	
	// Callback for stats amesties
	var eventNumber = function(xml) {

		var $xml = $(xml);
		var seriesOptions = {
				name: "Number of event by region",
				data: []
		};

		// Push categories
		$xml.find('region').each(function(i, language) {
			optionsBar.xAxis.categories.push($(language).text());
			seriesOptions.data.push(parseInt($(language).attr('count')));

		});


		optionsBar.series.push(seriesOptions);
		new Highcharts.Chart(optionsBar);
	};
	$.get('StatsEventByRegion', eventNumber).done(function(){});
	$.get('StatsClassifiedPlace', classifiedPlace).done(function(){});
});
</script>

<br/><br/>
<div id="pie" style="height:600px" class="pie"></div>
<br/><br/>
<div id="bar" style="height:800px" class="bar"></div>

<%@ include file="footer.html"%>    
